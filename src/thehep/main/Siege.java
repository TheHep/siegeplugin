package thehep.main;

import org.bukkit.plugin.java.JavaPlugin;

import ru.nullexception.lib.Debug;
import thehep.main.Commands;


public class Siege extends JavaPlugin {
	public final String pluginPrefix = "[SiegePlugin]";
	
	public void onEnable() {
		getLogger().info("Plugin enabled");
		RegisterCommands();
	}
	
	public void onDisable() {
		getLogger().info("Plugin disabled");
	}
	
	private void RegisterCommands() {	
		Debug.log(pluginPrefix, "RegisterCommands");
		Debug.log("--+------");
		
		try {
			getCommand("siege").setExecutor(new Commands());
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		Debug.log("--+------");
		Debug.log(pluginPrefix, "RegisterCommands complete");
	}
	
}
